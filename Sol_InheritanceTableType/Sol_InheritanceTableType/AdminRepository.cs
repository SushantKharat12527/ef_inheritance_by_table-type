﻿using Sol_InheritanceTableType.EF;
using Sol_InheritanceTableType.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_InheritanceTableType
{
   public class AdminRepository:PersonRepository
    {
        #region Constructor
        public AdminRepository():base()
        {

        }
        #endregion
        #region Public Method
        public async Task<IEnumerable<AdminEntity>> GetAdminDetail()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                         base.DbObject
                         ?.tblPersons
                         ?.OfType<tblAdmin>()
                         ?.AsEnumerable()
                         ?.Select(this.SelectAdminDetail)
                         ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func< tblAdmin, AdminEntity> SelectAdminDetail
        {
            get
            {
                return
                    (leTblAdminObj) => new AdminEntity()
                    {
                        PersonId= leTblAdminObj.Person_Id,
                        FirstName = leTblAdminObj.FirstName,
                        LastName = leTblAdminObj.LastName,
                        Salary = leTblAdminObj.Salary,
                        
                        //UserName=leTblAdminObj.UserName,
                        //Password=leTblAdminObj.Password
                       } ;
            }
        }

        #endregion
    }
}
