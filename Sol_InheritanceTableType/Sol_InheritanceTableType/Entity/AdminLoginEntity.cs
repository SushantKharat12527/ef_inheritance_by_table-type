﻿using Sol_EF_Table_Type_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_InheritanceTableType.Entity
{
    public class AdminLoginEntity:AdminEntity
    {
     
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
