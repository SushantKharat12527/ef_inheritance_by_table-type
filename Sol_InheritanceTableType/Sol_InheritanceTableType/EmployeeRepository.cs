﻿using Sol_EF_Table_Type_Inheritance.Entity;
using Sol_InheritanceTableType.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_InheritanceTableType
{
   public class EmployeeRepository:PersonRepository
    {
     
        #region Constructor
        public EmployeeRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<EmployeeEntity>> GetEmployeeData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        base.DbObject
                        ?.tblPersons
                        ?.OfType<tblEmp>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectEmployeeData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tblEmp, EmployeeEntity> SelectEmployeeData
        {
            get
            {
                return
                    (leTblEmployeeObj) => new EmployeeEntity()
                    {
                        PersonId = leTblEmployeeObj.Person_Id,
                        FirstName = leTblEmployeeObj.FirstName,
                        LastName = leTblEmployeeObj.LastName,
                        Salary = leTblEmployeeObj.Salary
                    };
            }
        }

        #endregion
    }
}
