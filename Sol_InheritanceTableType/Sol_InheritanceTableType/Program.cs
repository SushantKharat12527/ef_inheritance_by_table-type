﻿using Sol_EF_Table_Type_Inheritance.Entity;
using Sol_InheritanceTableType.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_InheritanceTableType
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<EmployeeEntity> listEmployeeObj =
                   await new EmployeeRepository().GetEmployeeData();




                IEnumerable<AdminEntity> listVendorObj =
                    await new AdminRepository().GetAdminDetail();

            }).Wait();
        }
    }
}
