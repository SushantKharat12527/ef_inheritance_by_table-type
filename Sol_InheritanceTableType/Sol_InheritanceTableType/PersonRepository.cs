﻿using Sol_InheritanceTableType.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_InheritanceTableType
{
   public class PersonRepository
    {
        #region Declaration
        private PersondbEntities db = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            db = new PersondbEntities();

            this.DbObject = db;
        }
        #endregion

        #region Property
        public PersondbEntities DbObject { get; set; }
        #endregion
    }
}
